function fire(x,y) {
  game.shoot(x,-y);
}

function setRocketMass(d) {
  game.setRocketMass(d);
}

function setGravity(x,y) {
  game.setWorldGravity(x,y);
}

function setDrag(d) {
  game.setRocketDrag(d);
}

function customBlockInit(Blockly) {
  Blockly.Blocks['event_shoot'] = {
    init: function() {
      this.setHelpUrl('http://www.example.com/');
      this.setColour(20);
      this.appendValueInput("forceX")
          .setCheck("Number")
          .appendField("forceX");
      this.appendValueInput("forceY")
          .setCheck("Number")
          .appendField("forceY");
      this.setInputsInline(true);
      this.setPreviousStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['event_shoot'] = function(block) {
    var value_forceX = Blockly.JavaScript.valueToCode(block, 'forceX', Blockly.JavaScript.ORDER_ATOMIC);
    var value_forceY = Blockly.JavaScript.valueToCode(block, 'forceY', Blockly.JavaScript.ORDER_ATOMIC);
    value_forceX = value_forceX === '' ? 0 : value_forceX;
    value_forceY = value_forceY === '' ? 0 : value_forceY;
    var code = 'game.shoot('+value_forceX+', '+value_forceY+');\n';
    return code;
  };

  Blockly.Blocks['horizontal_force_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Horizonal Force');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['50', '50'], ['100', '100']]);
      input.appendField(dropdown, 'horizontalForce');
    }
  };
  Blockly.JavaScript['horizontal_force_fixed'] = function(block) {
    var value_forceX = block.getFieldValue('horizontalForce');
    var code = 'var horizontalForce = '+value_forceX+';\n';
    return code;
  };

  Blockly.Blocks['vertical_force_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Vertical Force');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['50', '50'], ['100', '100']]);
      input.appendField(dropdown, 'verticalForce');
    }
  };
  Blockly.JavaScript['vertical_force_fixed'] = function(block) {
    var value_forceX = block.getFieldValue('verticalForce');
    var code = 'var verticalForce = '+value_forceX+';\n';
    return code;
  };

  Blockly.Blocks['fire_basic'] = {
    init: function() {
      this.setColour(20);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Fire');
    }
  };
  Blockly.JavaScript['fire_basic'] = function(block) {
    var code = 'fire(horizontalForce, verticalForce);\n';
    return code;
  };

  Blockly.Blocks['fire_fixed'] = {
    init: function() {
      this.setColour(20);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Fire');
    }
  };

  Blockly.JavaScript['fire_fixed'] = function(block) {
    var code = 'fire(100, 0);\n';
    return code;
  };

  Blockly.Blocks['gravity_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Gravity');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['50', '50'], ['100', '100']]);
      input.appendField(dropdown, 'gravityForce');
    }
  };
  Blockly.JavaScript['gravity_fixed'] = function(block) {
    var value = block.getFieldValue('gravityForce');
    var code = 'setGravity(0,'+value+');\n';
    return code;
  };

  Blockly.Blocks['gravity_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("gravity_variable")
          .appendField("Set Gravity");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['gravity_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'gravity_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'setGravity(0,'+value+');\n';
    return code;
  };

  Blockly.Blocks['drag_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Drag');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['5', '5'], ['10', '10']]);
      input.appendField(dropdown, 'drag');
    }
  };
  Blockly.JavaScript['drag_fixed'] = function(block) {
    var value = block.getFieldValue('drag');
    var code = 'setDrag('+value+');\n';
    return code;
  };

  Blockly.Blocks['drag_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("drag_variable")
          .appendField("Set Drag");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['drag_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'drag_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'setDrag('+value+');\n';
    return code;
  };

  Blockly.Blocks['mass_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Mass');
      var dropdown = new Blockly.FieldDropdown([['1', '1'], ['5', '5'], ['20', '20']]);
      input.appendField(dropdown, 'mass');
    }
  };
  Blockly.JavaScript['mass_fixed'] = function(block) {
    var value = block.getFieldValue('mass');
    var code = 'setRocketMass('+value+');\n';
    return code;
  };

  Blockly.Blocks['mass_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("mass_variable")
          .appendField("Set Mass");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['mass_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'mass_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'setRocketMass('+value+');\n';
    return code;
  };
}
